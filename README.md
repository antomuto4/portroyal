# [port royal]

[https://deepdive.gg/portroyal](https://deepdive.gg/portroyal)

This is a set of markdown files used to generate the [port royal] page with the tool [ssg6](https://www.romanzolotarev.com/ssg.html) by Roman Zolotarev.

## Ways to contribute

- By adding new sections that didn't exist yet.

- Fix errors in pre-existing sections or add improvements.

## How to contribute

- In order to contribute to [port royal], please read [Gitlab's guide on the workflow of Contribution](https://docs.gitlab.com/ee/development/contributing/#contribution-flow). If you do not merge request, we will be *unable* to add this to our site.

- Here you can see in GIF format how you can contribute and merge to our project.
## [Step 1] Fork

 ![fork](/uploads/8ba457f899afb9f3d53af6f9df5c92b0/fork.gif)

## [Step 2] Emerge Request

 ![merge-request](/uploads/bc4c4e6ea442a8ace00b27194d89979d/merge-request.gif)

## Rules for submission

- Make sure to not post any links that contain a virus or that are malicious in any other way.

- Do not submit duplicates or dead links. 


