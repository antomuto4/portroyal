### [manga / 漫画]
- [Dl-Raw](https://dl-raw.net/)
- [Raw Lazy](https://rawlazy.com/)

### [general media / 図書館 ]
- [Tokyo Toshokan / 東京 図書館](https://www.tokyotosho.info/) A BitTorrent Library for Japanese Media
- [AcgnX Torrent](https://www.acgnx.se/) Anime Comics Game Novel eXchange
- [AniDex](https://anidex.info/)
- [AniRena](https://www.anirena.com/)
- [Nyaa](https://nyaa.si/)

### [literature / 文献]
- [Aozora](https://www.aozora.gr.jp/)
