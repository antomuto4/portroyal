## [games]

###[general]
- [/r/CrackWatch](https://reddit.com/r/CrackWatch/) New video game crack releases are posted here
- [Beginners Guide to Crack Watch](https://www.reddit.com/r/CrackWatch/comments/a7l141/crack_watch_beginners_guide_to_crack_watch/) Reddit post by /u/EssenseOfMagic
- [ GOD scraped URLs](https://drive.google.com/file/d/17MB0gCcCMr3QqE_CgJkaxmdXtZk61TdZ/view) All DDL links for games listed on the now-dead GoodOldDownloads site.
- [Free GOG PC Games](http://freegogpcgames.com/) Magnet links site to download games from GOG.
- [cs.rin.ru](https://cs.rin.ru/) Popular gaming piracy forum
- [SmartSteamEmu](https://github.com/MAXBURAOT/SmartSteamEmu) Steam emulator
- [goldberg_emulator](https://gitlab.com/Mr_Goldberg/goldberg_emulator) Steam emulator that emulates steam online features. Lets you play games that use the steam multiplayer APIs on a LAN without steam or an internet connection
- [CreamAPI](https://cs.rin.ru/forum/viewtopic.php?t=70576) "A Legit DLC Unlocker" for Steam
- [cream-api-autoinstaller](https://github.com/Douile/cream-api-autoinstaller) A python script to automatically install Cream API for Steam games
- [CDRomance](https://cdromance.com/) PSP, PSX, PS2, Gameboy, NDS, SNES, Dreamcast, and Gamecube ROMs and ISOs.
- [redump.org](http://redump.org/) Disc preservation database and internet community dedicated to collecting precise and accurate information about every video game ever released on optical media of any system.
- [Steamless](https://github.com/atom0s/Steamless) Steamless is a DRM remover of the SteamStub variants.
- [MachineGunnur/GOG-Games](https://github.com/MachineGunnur/GOG-Games) A fork of Good Old Downloads' "GOG Games"

###[repacks]
- [FitGirl Repacks](http://fitgirl-repacks.site/) Popular DDL and torrent site for game repacks
- [Xatab Repacks](https://xatab-repack.net/) Russian game repacker, primarily torrents
- [ElAmigos Games](https://elamigos.site/) Premium links to cracked games
- [Nicoblog](https://nicoblog.org/) Plenty of ISOs, ROMs, and repacks
- [Dark Umbra](https://darkumbra.net/) Forum for sourcing games

###[roms]
- [Romsmania](https://romsmania.cc/) Good ROMs collection with a decent UI.
- [Doperoms](https://www.doperoms.com/) Huge collection with over 170,000 ROM files. PS3 included.
- [Vimm's Lair](https://vimm.net/?p=vault) Large collection of ROMs
- [ROM/ISO sites](http://emulation.gametechwiki.com/index.php/ROM_%26_ISO_Sites) Wiki page from gametechwiki.com with more links
- [Romulation.net](https://www.romulation.net/) Collection of ~28,000 console game ROMs
- [The Eye ROMs](http://the-eye.eu/public/rom/) Open directory of ROMs from The Eye
- [myabandonware](https://www.myabandonware.com/) More than 14000 old games to download for free!
- [Old Games Finder](http://www.oldgamesfinder.com/) Old Games Finder is an automated old game search engine.](avoid ISO Zone links, as that site is dead)
- [The ROM Depot](https://theromdepot.com/roms/) About 3TB of ROMs. You may need a VPN.
- [Emulator.Games](https://emulator.games/) Download or play ROMs on your PC, Mobile, Mac, iOS and Android devices.
- ["A simple script for easily downloading emulator.games roms"](https://www.reddit.com/r/Piracy/comments/aytutr/a_simple_script_for_easily_downloading/) Reddit guide and userscript created by /u/estel_smith to allow you to easily download ROMs from Emulator.Games.
- [3DSISO](http://www.3dsiso.com/) Nintendo 3DS ROMs downloads forum.
- [3DSCIA.com](https://www.3dscia.com/) DDL links for 3DS CIA files.
- [Nintendo 3DS Complete Collection](Archive.org)(https://archive.org/details/nintendo-3ds-complete-collection)
- [Ziperto](https://www.ziperto.com/nintendo/3ds-roms/3ds-cia/) DDL link site primarily for Nintendo games.
- [Edge Emulation](https://edgeemu.net/) A very clean looking website that has all the ROMs up until the Sega Dreamcast.
